package com.example.ZentityBooks.CustomObjects;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context){

            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
                    cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),APP_Config.SOURCES.CACHE_DIRNAME);
            }  else   {
                    cacheDir=context.getCacheDir();
            }
            if(!cacheDir.exists()){
                    Log.d(APP_Config.MAIN.APP_NAME, "makeDir: " + cacheDir.mkdirs());
            }
    }

    public File getFile(String url){

            String filename=String.valueOf(url.hashCode());
            File f = new File(cacheDir, filename);
            return f;

    }


    public void clear(){
            File[] files=cacheDir.listFiles();
            if(files==null)
                return;
            for(File f:files)
                f.delete();
    }

}