package com.example.ZentityBooks.CustomObjects;

public class APP_Config{

    //definition constatns

    public static class MAIN{
            public static final String APP_NAME = "ZentityBooks";
            public static final String COUNT_PORTRAIT= "booksPerRow_portrait";
            public static final String COUNT_LANDSCAPE = "booksPerRow_landscape";
            public static final String BOOK_DETAIL = "bookDetail";
    }

    public static class SOURCES{
            public static final String BOOKS_URL = "http://www.lukaspetrik.cz/filemanager/tmp/reader/data.xml";
            public static final String CACHE_DIRNAME = "appcache";
    }

    public static class DEFAULT_COUNTS{
            public static final int PORTRAIT = 2;
            public static final int LANDSCAPE = 3;
    }

    public static class CODE{
            public static final int PERM_REQUEST = 555;
            public static final int SETTINGS_REQUEST = 888;
            public static final int BOOK_ACTIVITY_DETAIL = 666;
    }

}
