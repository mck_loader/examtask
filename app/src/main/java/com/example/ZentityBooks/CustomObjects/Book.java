package com.example.ZentityBooks.CustomObjects;


public class Book  {

    private String Id;
    private String Title;
    private String Thumb_url;
    private Boolean isNew;

    public Book(String _id, String _title, String _thumb_url, Boolean _isNew){
        this.Id=_id;
        this.Title=_title;
        this.isNew=_isNew;
        this.Thumb_url=_thumb_url;

    }

    public Book(){
        this.Id="";
        this.Title="";
        this.isNew=false;
        this.Thumb_url="";

    }

    public void setId(String _id){
        this.Id=_id;
    }
    public void setTitle(String _title){
        this.Title=_title;
    }
    public void setIsNew(Boolean _isNew){
        this.isNew=_isNew;
    }
    public void setThumbUrl(String _url){
        this.Thumb_url=_url;
    }



    public String getId(){
        return this.Id;
    }
    public String getTitle(){
        return this.Title;
    }
    public String getThumbUrl(){
        return this.Thumb_url;
    }
    public Boolean getIsNew(){
        return this.isNew;
    }




}
