package com.example.ZentityBooks.CustomObjects;


import android.os.AsyncTask;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BookLoader extends AsyncTask<Void,Integer,ArrayList<Book>> {

    private onBookLoad obl = null;
    private XmlPullParserFactory parserFactory;
    private int httpCode = 999;
    private String statusDesc = "N/A";

    public BookLoader(onBookLoad _obl) {
        this.obl = _obl;
    }


    @Override
    protected ArrayList<Book> doInBackground(Void... params) {

        ArrayList<Book> books = null;
        InputStream inputStream = null;

        try {

                URL url = new URL(APP_Config.SOURCES.BOOKS_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");

                try{
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setChunkedStreamingMode(0);
                            httpCode=httpURLConnection.getResponseCode();

                            if (httpCode == HttpURLConnection.HTTP_OK) {
                                            inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                                            try {
                                                parserFactory = XmlPullParserFactory.newInstance();
                                                XmlPullParser parser = parserFactory.newPullParser();
                                                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                                                parser.setInput(inputStream, "UTF-8");
                                                books=  parseXML(parser);

                                            } catch (XmlPullParserException e) {
                                                statusDesc=e.getClass().getName()+" error";
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                statusDesc=e.getClass().getName()+" error";
                                                e.printStackTrace();
                                            }
                            }else {
                                statusDesc=httpURLConnection.getResponseMessage();
                                inputStream = new BufferedInputStream(httpURLConnection.getErrorStream());
                            }


                }catch (Exception e){
                        statusDesc=e.getClass().getName()+" error";
                        e.printStackTrace();
                }finally {
                        httpURLConnection.disconnect();
                        if (inputStream!=null)      inputStream.close();
                   }
        } catch (Exception e){
            statusDesc=e.getClass().getName()+" error";
            e.printStackTrace();
        }

        return books;
    }


    @Override
    protected void onPostExecute(ArrayList<Book> books) {
        if(books!=null) obl.onSuccess(books);
        else obl.onError(httpCode,statusDesc);
    }

    private ArrayList<Book> parseXML(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        ArrayList<Book> books = null;
        int eventType = parser.getEventType();
        Book book = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                        books = new ArrayList();
                        break;
                case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("BOOK")){
                              book = new Book();
                        } else if (book != null){
                                if (name.equals("ID")){
                                    book.setId(parser.nextText());
                                }
                                else if (name.equals("TITLE")){
                                    book.setTitle(parser.nextText());
                                }
                                else if (name.equals("NEW")){
                                    book.setIsNew(Boolean.parseBoolean(parser.nextText()));
                                }
                                else if (name.equals("THUMBNAIL")){
                                    book.setThumbUrl(parser.nextText());
                                }
                        }
                        break;
                case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("BOOK") && book != null){
                            books.add(book);
                        }
                         break;
            }
            eventType = parser.next();
        }

        return books;

    }

}
