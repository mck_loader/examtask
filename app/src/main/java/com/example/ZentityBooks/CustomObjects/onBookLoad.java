package com.example.ZentityBooks.CustomObjects;



import java.util.ArrayList;

public interface onBookLoad {

    void onError(int err_code, String err_desc);

    void onSuccess(ArrayList<Book> books);

}
