package com.example.ZentityBooks.CustomObjects;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.GridView;

import static com.example.ZentityBooks.R.drawable.shelfcell_background;

public class BookShelf extends GridView {

    private Bitmap background;
    private Context ctx;

    public BookShelf(Context context, AttributeSet attrs){
            super(context,attrs);
            ctx=context;
            background = BitmapFactory.decodeResource(getResources(), shelfcell_background);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) ctx).getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            background= Bitmap.createScaledBitmap(background, width, getPXs(174), true);

    }


    public  int getPXs( final int dp) {
            return  Math.round(dp * ctx.getResources().getDisplayMetrics().density);
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
            int top = getChildCount() > 0 ? getChildAt(0).getTop() : 0;
            for (int y = top; y < getHeight(); y += background.getHeight()) {
                for (int x = 0; x < getWidth(); x += background.getWidth()) {
                    canvas.drawBitmap(background, x, y, null);
                }
            }
            super.dispatchDraw(canvas);
    }



}