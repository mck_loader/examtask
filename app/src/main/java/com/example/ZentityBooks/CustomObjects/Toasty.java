package com.example.ZentityBooks.CustomObjects;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ZentityBooks.R;

public class Toasty {
    public static final int ERROR = -1;
    public static final int SUCCESS = 1;
    public static final int INFO = 0;
    public static final int WARN = 2;
    public static final int SHORTTIME = Toast.LENGTH_SHORT;
    public static final int LONGTIME = Toast.LENGTH_LONG;
    public Toasty(Context _source, String _message, int _duration, int _style){
        LayoutInflater inflater = (LayoutInflater) _source.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Toast t=Toast.makeText(_source,_message,_duration);
        View toastRoot = inflater.inflate(R.layout.toasty_err, null);


        switch (_style) {
            case ERROR:
                toastRoot = inflater.inflate(R.layout.toasty_err, null);
                break;
            /*case SUCCESS:
                toastRoot = inflater.inflate(R.layout.toast_lay_success, null);
                break;*/
            case WARN:
                toastRoot = inflater.inflate(R.layout.toasty_warn, null);
                break;
           /* case INFO:
                toastRoot = inflater.inflate(R.layout.toast_lay, null);
                break;*/
            default:
                break;

        }

        TextView tv=(TextView) toastRoot.findViewById(R.id.toasty_mess);
        tv.setText(_message);
        t.setView(toastRoot);
        t.show();

    }


}
