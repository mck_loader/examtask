

package com.example.ZentityBooks;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ZentityBooks.BookDetail;
import com.example.ZentityBooks.CustomObjects.APP_Config;
import com.example.ZentityBooks.CustomObjects.Book;
import com.example.ZentityBooks.CustomObjects.BookLoader;
import com.example.ZentityBooks.CustomObjects.GridAdapter;
import com.example.ZentityBooks.CustomObjects.Toasty;
import com.example.ZentityBooks.CustomObjects.onBookLoad;
import com.example.ZentityBooks.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class Main extends AppCompatActivity {

    private int count;
    Dialog restoreDialog;
    View customView;
    RelativeLayout perm_issue;
    private SharedPreferences sp;
    Snackbar settingsSnackbar=null;
    ArrayList<Book> books;
    Boolean started = false,  isCountChanged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.sp= getSharedPreferences(APP_Config.MAIN.APP_NAME, MODE_PRIVATE);
        setContentView(R.layout.main);
        restoreDialog=new Dialog(this);
        settingsSnackbar= Snackbar.make(findViewById(R.id.root),"XXX", Snackbar.LENGTH_INDEFINITE);
    }



    private void checkPermissionAndRun(){

        //Checking internet and storage access permissions with controling by versionOS
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.d(APP_Config.MAIN.APP_NAME, "wrong: M");
                requestPermissions(new String[]{Manifest.permission.INTERNET,Manifest.permission.WRITE_EXTERNAL_STORAGE}, APP_Config.CODE.PERM_REQUEST);
            }
            else runBL();
        }
        else if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            if(PermissionChecker.checkSelfPermission(this,  Manifest.permission.INTERNET) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkSelfPermission(this,  Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ){
                new Toasty(this, "Internet permission or storage permission not granted!",Toasty.LONGTIME, Toasty.ERROR);
            }
            else runBL();
        }
    }


    private void runBL(){
        //async download XML
        BookLoader bl=new BookLoader(new onBookLoad() {
            @Override
            public void onError(int err_code, String err_desc) {
                new Toasty(getApplicationContext(),"["+err_code+"] "+err_desc,Toasty.LONGTIME, Toasty.ERROR);
            }

            @Override
            public void onSuccess(ArrayList<Book> _books) {
                books=_books;
                //if successfully downloaded and parsed show shelves
                show();
            }
        });
        bl.execute();

    }


    @Override
    protected void onResume() {
        //check just one time
        if(!started){
            checkPermissionAndRun();
            started=true;
        }

        super.onResume();
    }


    private void show(){

            //Getting counts
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                this.count = sp.getInt(APP_Config.MAIN.COUNT_LANDSCAPE, APP_Config.DEFAULT_COUNTS.LANDSCAPE);
            } else {
                this.count = sp.getInt(APP_Config.MAIN.COUNT_PORTRAIT, APP_Config.DEFAULT_COUNTS.PORTRAIT);
            }

            GridView gridview = (GridView) findViewById(R.id.gridview);
            gridview.setNumColumns(this.count);

            //attach custom adapter
            GridAdapter gridAdapter = new GridAdapter(getApplicationContext(), books);
            gridview.setAdapter(gridAdapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                          showDetail(books.get(position), view);
                }
            });



    }

    // Snackbar with settings
    public void settings(View v){

            settingsSnackbar = Snackbar.make(findViewById(R.id.root),
                    "XXX", Snackbar.LENGTH_INDEFINITE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            Snackbar.SnackbarLayout layout= (Snackbar.SnackbarLayout) settingsSnackbar.getView();
            layout.removeAllViews();
            layout.setBackgroundColor(getResources().getColor(R.color.black));
            LayoutInflater own = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView = own.inflate(R.layout.snack_settings, null);// use the parent ViewGroup instead of null        // Configure the view
            perm_issue= (RelativeLayout) customView.findViewById(R.id.perms_sec);



            TextView land_value= (TextView) customView.findViewById(R.id.tv_landscape_value);
            land_value.setText(String.valueOf(sp.getInt(APP_Config.MAIN.COUNT_LANDSCAPE,APP_Config.DEFAULT_COUNTS.LANDSCAPE)));
            Button land_plus = (Button) customView.findViewById(R.id.btn_landscape_plus);
            land_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    set_landscape(1);
                }
            });
            Button land_minus = (Button) customView.findViewById(R.id.btn_landscape_minus);
            land_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    set_landscape(-1);
                }
            });

            TextView port_value= (TextView) customView.findViewById(R.id.tv_portrait_value);
            port_value.setText(String.valueOf(sp.getInt(APP_Config.MAIN.COUNT_PORTRAIT,APP_Config.DEFAULT_COUNTS.PORTRAIT)));
            Button port_plus = (Button) customView.findViewById(R.id.btn_portrait_plus);
            port_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    set_portrait(1);
                }
            });
            Button port_minus = (Button) customView.findViewById(R.id.btn_portrait_minus);
            port_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    set_portrait(-1);
                }
            });
            Button close = (Button) customView.findViewById(R.id.btn_close);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideSettings();
                }
            });

            TextView restore= (TextView) customView.findViewById(R.id.tv_restore);
            restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    restoreConfirm();
                }
            });

            layout.setPadding(0, 0, 0, 0);
            layout.addView(customView,0);
            settingsSnackbar.show();
            checkPermIssueInSettings();

    }


    private void set_landscape(int direction){
        //set new value by direction - update Textview and set value in SharedPref
        //minimum items per row is 1 (0 has no sense)
        TextView land_value = (TextView) findViewById(R.id.tv_landscape_value);
        if(direction==0){
            sp.edit().putInt(APP_Config.MAIN.COUNT_LANDSCAPE, APP_Config.DEFAULT_COUNTS.LANDSCAPE).commit();
            land_value.setText(String.valueOf(APP_Config.DEFAULT_COUNTS.LANDSCAPE));
        }
        else {
            int val = Integer.parseInt(land_value.getText().toString());
            int new_val = val + direction;
            if(new_val>=1) {
                land_value.setText(String.valueOf(new_val));
                sp.edit().putInt(APP_Config.MAIN.COUNT_LANDSCAPE, new_val).commit();
            }
            else {
                new Toasty(this,"Is not allowed to set value to less than 1!", Toasty.SHORTTIME, Toasty.ERROR);
                return;
            }
        }
        isCountChanged = true;
    }


    private void set_portrait(int direction){
        //set new value by direction - update Textview and set value in SharedPref
        //minimum items per row is 1 (0 has no sense)
        TextView port_value= (TextView) findViewById(R.id.tv_portrait_value);
        if(direction==0){
            sp.edit().putInt(APP_Config.MAIN.COUNT_PORTRAIT, APP_Config.DEFAULT_COUNTS.PORTRAIT).commit();
            port_value.setText(String.valueOf( APP_Config.DEFAULT_COUNTS.PORTRAIT));
        }
        else {
            int val = Integer.parseInt(port_value.getText().toString());
            int new_val = val + direction;
            if(new_val>=1) {
                port_value.setText(String.valueOf(new_val));
                sp.edit().putInt(APP_Config.MAIN.COUNT_PORTRAIT, new_val).commit();
            }
            else {
                new Toasty(this,"Is not allowed to set value to less than 1!", Toasty.SHORTTIME, Toasty.ERROR);
                return;
            }
        }
        isCountChanged=true;
    }

    //Dialog for confirm  to reset counts to default values
    private void restoreConfirm(){

            restoreDialog=new Dialog(this);
            restoreDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            restoreDialog.setContentView(R.layout.restore_dialog);
            Button  btn_negative = (Button) restoreDialog.findViewById(R.id.dialog_btn_negative);
            btn_negative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    restoreDialog.dismiss();
                }
            });
            Button  btn_positive = (Button) restoreDialog.findViewById(R.id.dialog_btn_positive);
            btn_positive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    set_landscape(0);
                    set_portrait(0);
                    restoreDialog.dismiss();
                }
            });


            restoreDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            restoreDialog.show();
    }


    private void hideSettings(){
        settingsSnackbar.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        if(isCountChanged) show();
    }


    //BackBTN behavior while showing Dialog od snackbar (dialog is inner -> 1st in condition)
    @Override
    public void onBackPressed() {
        if(restoreDialog.isShowing()) restoreDialog.dismiss();
        else  if(settingsSnackbar.isShown()) hideSettings();
        else super.onBackPressed();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case APP_Config.CODE.PERM_REQUEST:
                    // if request is cancelled,  result arrays are empty.
                    if (grantResults.length > 1 &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED  &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                            runBL();
                    }
                    else if (grantResults.length > 1 &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            new Toasty(this, "Storage permission not granted!",Toasty.SHORTTIME, Toasty.WARN);
                            runBL();

                    }  else {
                            new Toasty(this, "No permissions are granted!",Toasty.SHORTTIME, Toasty.ERROR);
                    }
        }
        checkPermIssueInSettings();
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
            //for communication between rotations
            Gson gson = new Gson();
            Log.d(APP_Config.MAIN.APP_NAME, "onRestoreInstanceState: "+savedInstanceState.getString("books"));
            books= gson.fromJson(savedInstanceState.getString("books"), new TypeToken<ArrayList<Book>>() {}.getType());
            started=savedInstanceState.getBoolean("started");
            show();
            super.onRestoreInstanceState (savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState (Bundle outState) {
            //For communication between rotations
            super.onSaveInstanceState (outState);
            Gson gson = new Gson();
            String jsonBooks = gson.toJson(books);
            Log.d(APP_Config.MAIN.APP_NAME, "onSaveInstanceState: "+jsonBooks);
            outState.putString ("books", jsonBooks);
            outState.putBoolean ("started", started);
    }




    public void showDetail(Book book, View view){

        Intent intent = new Intent(getApplicationContext(), BookDetail.class);
        Gson gson = new Gson();
        String jsonBooks = gson.toJson(book);
        intent.putExtra(APP_Config.MAIN.BOOK_DETAIL, jsonBooks);
        Bundle bundle=null;

        //checking versionOS - transition support
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.img_book), view.findViewById(R.id.img_book).getTransitionName());
            bundle= ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, p1).toBundle();
        }
        start_activity_for_result(intent, APP_Config.CODE.BOOK_ACTIVITY_DETAIL, bundle );

    }

    public void  start_activity_for_result(Intent intent_, int result_type, Bundle b){

        startActivityForResult(intent_,result_type,b);
        //Checking versionOS - transition support
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else {
            // Swap without transition
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==APP_Config.CODE.SETTINGS_REQUEST){
            checkPermIssueInSettings();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void checkPermIssueInSettings() {
                //To control showing permission issue  with action -> Mashmallow & above requestPermissions(), below start phone settings
                if (settingsSnackbar.isShownOrQueued()) {
                            TextView tv_solve = (TextView) perm_issue.findViewById(R.id.tv_btn_solve);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    tv_solve.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            requestPermissions(new String[]{Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE}, APP_Config.CODE.PERM_REQUEST);
                                        }
                                    });

                                } else perm_issue.setVisibility(View.GONE);
                            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                if (PermissionChecker.checkSelfPermission(this, Manifest.permission.INTERNET) != PermissionChecker.PERMISSION_GRANTED ||
                                        PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                                    tv_solve.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            start_activity_for_result(new Intent(Settings.ACTION_SETTINGS), APP_Config.CODE.SETTINGS_REQUEST, new Bundle());
                                        }
                                    });

                                } else perm_issue.setVisibility(View.GONE);
                            }

                }
    }
}