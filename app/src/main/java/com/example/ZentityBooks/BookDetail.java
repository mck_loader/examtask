package com.example.ZentityBooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ZentityBooks.CustomObjects.APP_Config;
import com.example.ZentityBooks.CustomObjects.Book;
import com.example.ZentityBooks.CustomObjects.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class BookDetail extends AppCompatActivity {

    Book book;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_book_detail);
            ImageView img = (ImageView) findViewById(R.id.img_book);
            Intent intent = getIntent();
            String jsonBook = intent.getExtras().getString(APP_Config.MAIN.BOOK_DETAIL);
            Gson gson = new Gson();
            book= gson.fromJson(jsonBook, Book.class);
            ImageLoader imageLoader=new ImageLoader(this);
            imageLoader.DisplayImage(book.getThumbUrl(), img);

    }
}