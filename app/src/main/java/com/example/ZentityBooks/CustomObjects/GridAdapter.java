package com.example.ZentityBooks.CustomObjects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ZentityBooks.R;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    private Context context;
    private int logos[];
    private ArrayList<Book> books;
    private LayoutInflater inflter;

    public GridAdapter(Context _ctx, ArrayList<Book> _books) {
        this.context = _ctx;
        this.books = _books;
        inflter = (LayoutInflater.from(_ctx));
    }


    @Override
    public int getCount() {
            return books.size();
    }


    @Override
    public Object getItem(int pos) {
            return books.get(pos);
    }


    @Override
    public long getItemId(int i) {
            return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflter.inflate(R.layout.bookcell, null);
            TextView title_text = (TextView) view.findViewById(R.id.title_book);
            ImageView img = (ImageView) view.findViewById(R.id.img_book);

            if(books.get(i).getIsNew()) {
                ImageView label = (ImageView) view.findViewById(R.id.newLabel);
                label.setVisibility(View.VISIBLE);
            }

            title_text.setText(books.get(i).getTitle());
            ImageLoader imageLoader=new ImageLoader(context);
            imageLoader.DisplayImage(books.get(i).getThumbUrl(), img);

            return view;
    }
}